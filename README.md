# Stripe-DRF-Payment-Flow-Template
A template of a full 3d-secure payment flow for recieving payments with **Stripe**'s *Payment Intent* objects with a *Django Rest Framework* backend and a Vanilla JS/Bootstrap 5 frontend.

This was originally intended as a service to make additional payments beyond a main transaction linked to a service (i.e: the client payed for a basic service but now wants to add some extra-features, therefore needing him/she to pay an extra-amount)

## Includes
> A payment form rendered using the Django template engine that uses Vanilla JS to handle frontend interaction for Stripe.
> A Transaction and a Currency Django model to create payment objects.  
> The following endpoints:
    > CRUD endpoints for a Currency model
    > CRUD endpoints for a Transaction model
    > Form Detail endpoint for rendering payment form data
    > Charge Endpoint

## Does not include
> A logging system to register a history of the transaction flow
> An email flow to inform those involved in the transaction (payer and transaction owner)

## In progress
> Proper tests