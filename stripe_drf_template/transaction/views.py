from django.shortcuts import render

def template(request, crypto):

    """
    View that renders an appropiate form for the client to make a
    payment with the specs (amount, currency) indicated.

    If the transaction has already been completed, it will show this to
    the traveller.
    """
    
    
    return render(request, 'transaction/template.html')