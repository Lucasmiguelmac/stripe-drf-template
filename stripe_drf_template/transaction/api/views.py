from django.conf import settings

from django.utils.translation import gettext as _

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
import stripe

from ..models import Currency, Transaction
from stripe_drf_template.common.utils import (
    decrypt,
    serialized_data
)
from .serializers import (
    CurrencySerializer,
    TransactionListDetailSerializer,
    TransactionCreateSerializer,
    TransactionUpdateSerializer
)



@api_view(['GET'])
def api_overview(request):

    """
    List of endpoints available for the API
    """

    api_urls = {

        'Transaction List': 'transactions/',
        'Create Transaction': 'transactions/create/',
        'Update Transaction': 'transactions/<crypto>/update/',
        'Delete Transaction': 'transactions/<crypto>/delete/',


        'Currency List': 'currencies/',
        'Create Currency': 'currencies/create/',
        'Update Currency': 'currencies/<crypto>/update/',
        'Delete Currency': 'currencies/<crypto>/delete/',

    }
    return Response(api_urls)


##############################################################################
##############################################################################

@api_view(['GET'])
def currency_list(request):
    currencies = Currency.objects.all()
    serialized_data = CurrencySerializer(currencies, many=True).data
    return Response(serialized_data(serialized_data))


@api_view(['POST'])
def currency_create(request):
    serializer = CurrencySerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)
    else:
        data= {
            'error': 'Currency couldn\'t be created',
        }
        return Response(data=data, status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT'])
def currency_update(request, code): 
    try:
        try:
            currency = Currency.objects.get(code=code)
        except Currency.DoesNotExist:
            return Response({
                'error': {
                    'code': 404,
                    'message': 'Currency not found',
                }
            }, status=status.HTTP_404_NOT_FOUND)

        # Update a currency
        serializer = CurrencySerializer(currency, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_202_ACCEPTED)
    except Exception as e:
        return Response(
                data ={
                    'message': 'Currency not found',
                }, status=status.HTTP_409_CONFLICT)
    

@api_view(['DEL'])
def currency_delete(request, code):
    try:
        currency = Currency.objects.get(code=code)
    except Currency.DoesNotExist:
        return Response({
            'error': {
                'code': 404,
                'message': 'Currency not found',
            }
        }, status=status.HTTP_404_NOT_FOUND)

    # Delete a currency
    serializer = CurrencySerializer(currency)
    serializer.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)


##############################################################################
##############################################################################


@api_view(['GET'])
def transaction_list(request):
    transactions = Transaction.objects.all()
    serializer = TransactionListDetailSerializer(transactions, many=True)
    return Response(serialized_data(serializer))

   
@api_view(['POST'])
def transaction_create(request):

    """
    'currency' is passed as an int that matches a pk.
    """

    data = request.data

    # We make sure the ref_code has a valid length
    if data.get('ref', False):
            data['ref'] = str(data['ref'])[0:30]
            
    serializer = TransactionCreateSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
def transaction_update(request, crypto):
    """
    API that handles the overriding of the mutable fields of the Transaction
    model, that is: 'name', 'email', 'payment_status' & 'message'.

    'payment_status' recieves a string: 'WAI', 'SUC', 'FAI' or 'PEN'. 
    """
    
    try:
        pk = decrypt(crypto)
        transaction = Transaction.objects.get(pk=pk)
    except Transaction.DoesNotExist:
        return Response({
            'error': {
                'code': 404,
                'message': 'Transaction not found',
            }
        }, status=status.HTTP_404_NOT_FOUND)

    serializer = TransactionUpdateSerializer(data=request.data)

    if serializer.is_valid():
        transaction.name = request.data['name']
        transaction.email = request.data['email']
        transaction.message = request.data['message']
        transaction.payment_status = request.data['payment_status']
        transaction.save()      
        return Response(status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_412_PRECONDITION_FAILED)

        
@api_view(['DELETE'])
def transaction_delete(request, crypto):
    try:
        pk = decrypt(crypto)
        transaction = Transaction.objects.get(pk=pk)
    except Exception:
        return Response({
            'error': {
                'code': 404,
                'message': crypto,
            }
        }, status=status.HTTP_404_NOT_FOUND)

    # Delete a transaction
    transaction.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def transaction_detail(request, crypto):

    try:
        pk = decrypt(crypto)
        transaction = Transaction.objects.get(pk=pk)
    except Transaction.DoesNotExist:
        return Response({
            'error': {
                'code': 404,
                'message': 'Transaction not found',
            }
        }, status=status.HTTP_404_NOT_FOUND)

    serializer = TransactionListDetailSerializer(transaction)
    return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['GET'])
def transaction_form_detail(request, crypto):
    """
    FRONTEND:
    View that delivers the needed info to either
    - Render a proper card-payment form
    - Show the user a success message to prevent him/she from double-paying
    This view does not use a serializer.
    """
    try:
        pk = decrypt(crypto)
        transaction = Transaction.objects.get(pk=pk)
    except Exception:
        # Could not get transaction object
        data = {
            'error': 'Couldn\'t find a transaction associated to this url',
        }
        return Response(data=data, status=status.HTTP_204_NO_CONTENT)

    if transaction.payment_status == 'SUC':
        # Payment already suceeded
        data = {
            'success': 'This payment was already successfully submitted!',
        }
        return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)

    data = {
        
        'crypto_id': crypto,
        'currency_name': transaction.currency.name,
        'currency_code': transaction.currency.code.upper(),
        'currency_symbol': transaction.currency.symbol,
        'custom_message': transaction.message,
        'email': transaction.email,
        'name': transaction.name,
        'price_final': f'{transaction.amount:.2f}',
        'stripe_public_key': settings.STRIPE_PUBLIC_KEY,
        
    }
    return Response(data=data, status=status.HTTP_200_OK)


@api_view(['POST'])
def transaction_charge(request):
    """
    View that processes the the card info and returns to the frontend
    if the transaction fails or requires action from the payer due to
    3D secure authentication being required by the card's country.
    """
    
    try:
        
        # Retrieve transaction object
        form = request.POST
        try:
            crypto      = form['crypto_id']
            pk          = decrypt(crypto)
            transaction = Transaction.objects.get(pk=pk)
        except Exception as e:
            data = {
                'error': _('Couldn\'t find a transaction associated to this url'),
            }
            return Response(data=data, status=status.HTTP_204_NO_CONTENT)

        # Avoid double-payment beforehand
        if transaction.payment_status == 'SUC':
            # Transaction was succesful the first time but somehow we got our client to this endpoint twice
            data = {
                'success': _('Your transaction has successfully been done.')
            }
            return Response(data=data, status=status.HTTP_200_OK)

        # Transaction's payment status isn't 'SUC' (succesful)        
        else:

            stripe.api_key = settings.STRIPE_SECRET_KEY
            payment_intent = stripe.PaymentIntent.retrieve(
                transaction.payment_intent_id,
            )
            

            # Payment Status isn't succesful yet because we just haven't updated it (mostly due to european customers being redirected here due to 3dsec)
            if payment_intent.status == 'succeeded':
                transaction.payment_status = 'SUC'
                transaction.payment_charge_id = payment_intent.description
                transaction.save()
                data = {
                    'success': _('Your transaction has successfully been done.')
                }
                return Response(data=data, status=status.HTTP_200_OK)

            # Payment Status isn't succesful beacuse the Payment Intent hasn't finished it's lifecycle yet
            else:
                
                # Add the card in the form to the payment intent
                payment_method = form['payment_method_id']
                
                # Try to make an intent with the form and handle errors
                try:

                    # We fill the transaction object with the last info available (submitted by the client inside the form)
                    transaction.billing_email   = form.get('billing-email', transaction.email) # transaction email may differ from the email that originated the transactino (ie: someone requests to add a friend to the tour and he proceeds to pay with his/her credit card)
                    transaction.billing_name    = form.get('billing-name', transaction.name) # same here
                    
                    # We add a payment_method (card) and billing info to the payment intent
                    stripe.PaymentIntent.modify(
                        transaction.payment_intent_id,
                        payment_method=payment_method,
                        metadata={
                            'booking_owner_name': transaction.name,
                            'booking_owner_email': transaction.email,
                            'quickpay_billing_name': transaction.billing_name,
                            'quickpay_billing_email': transaction.billing_email,
                        }

                    )
                    payment_intent = stripe.PaymentIntent.confirm(
                        transaction.payment_intent_id,
                    )
                    
                    transaction.stripe_response = payment_intent
                    transaction.payment_status  = payment_intent.status[:3].upper() # 'FAI', 'SUC' or 'REQ'
                    transaction.save()
                    
                    # If there's no Stripe-related error, proceed to see if 3d sec is needed
                    if payment_intent.status == 'requires_source_action' or payment_intent.status == 'requires_action': # We have two options depending on the recentness of the stripe keys. See: 
                        pi = stripe.PaymentIntent.retrieve(
                            transaction.payment_intent_id
                        )
                        data = {
                            'stripe_public_key': settings.STRIPE_PUBLIC_KEY,
                            'payment_intent_secret': pi.client_secret,
                            'crypto_id': form['crypto_id'],
                        }
                        return Response(data=data, status=status.HTTP_202_ACCEPTED)
                    
                    # Otherwise, we proceed to confirm the payment was succesfully processed
                    transaction.payment_status      = payment_intent.status[:3].upper()
                    transaction.save()
                    if transaction.payment_status == 'SUC':
                        data = {
                            'success': _('Your transaction has successfully been done.')
                        }
                        return Response(data=data, status=status.HTTP_200_OK)

                # Handle error exception
                except stripe.error.CardError as e:
                    # Error handling for different card_declined errors
                    if e.error.code == 'card_declined':
                        if e.error.decline_code == 'insufficient_funds':
                            data = {
                                'message': _("The introduced card has insufficient funds."),
                            }
                            return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                        elif e.error.decline_code == 'lost_card':
                            data = {
                                'message': _("The introduced card was declared lost."),
                            }
                            return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                        elif e.error.decline_code == 'stolen_card':
                            data = {
                                'message': _("The introduced card was declared stolen."),
                            }
                            return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                        else:
                            data = {
                                'message': _("Your card has been declined, please try another card or contact your card company."),
                            }
                            return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                    elif e.error.code == 'expired_card':
                        data = {
                                'message': _("The introduced card has expired."),
                        }
                        return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                    elif e.error.code == 'incorrect_cvc':
                        data = {
                                'message': _("The introduced provided card information invalid. Please enter it again."),
                        }
                        return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                    elif e.error.code == 'processing_error':
                        data = {
                                'message': _("There was an error when processing your payment. Please try again or contact your card company."),
                        }
                        return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                    elif e.error.code == 'incorrect_number':
                        data = {
                                'message': _("The introduced card number is invalid."),
                        }
                        return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)

                except stripe.error.RateLimitError as e:
                    # Too many requests made to the API too quickly
                    data = {
                        'message': _('You made too many request too quick.'),
                    }
                    return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                except stripe.error.InvalidRequestError as e:
                    # Invalid parameters were suppled to Stripe, maybe the payment is already done.
                    data = {
                        'message': _('Invalid parameters were supplied.'),
                    }
                    return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                except stripe.error.AuthenticationError as e:
                    # Authentication with Stripe API failed, maybe Stripe keys were updated
                    data = {
                        'message': _('There was an internal error. Please notify a Lokafy representative.'),
                    }
                    return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                except stripe.error.APIConnectionError as e:
                    # Network communication with Stripe Failed
                    data = {
                        'message': _('There was an internal error. Please notify a Lokafy representative.'),
                    }
                    return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
                except stripe.error.StripeError as e:
                    data = {
                        'message': _('There was an internal error. Please notify a Lokafy representative.'),
                    }
                    return Response(data=data, status=status.HTTP_206_PARTIAL_CONTENT)
            
                except Exception as e:
                    data = {
                        'message': _('There was an internal error. Please notify a Lokafy representative.'),
                    }
                    return Response(data=data, status=status.HTTP_409_CONFLICT)
                
    except Exception as e:
        data = {
            'message': _('There was an internal error. Please notify a Lokafy representative.'),
        }
        # Info that can be logged
        error_type      = e.error.type
        error_code      = e.error.code
        error_message   = e.error.message
        return Response(data=data, status=status.HTTP_409_CONFLICT)