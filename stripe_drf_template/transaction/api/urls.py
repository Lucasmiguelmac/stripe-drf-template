from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from . import views

app_name = 'transaction'

urlpatterns = [

    path('', views.api_overview),

    path('transactions/', views.transaction_list, name='transaction_list'),
    path('transactions/form/<path:crypto>', views.transaction_form_detail, name='transaction_form_detail'),
    path('transactions/charge/', views.transaction_charge, name='transaction_charge'),
    path('transactions/create/', views.transaction_create, name='transaction_create'),
    path('transactions/update/<path:crypto>/', views.transaction_update, name='transaction_update'),
    path('transactions/delete/<path:crypto>/', views.transaction_delete, name='transaction_delete'),
    path('transactions/<path:crypto>/', views.transaction_detail, name='transaction_detail'),

    path('currencies/', views.currency_list, name='currency_list'),
    path('currencies/create/', views.currency_create, name='currency_create'),
    path('currencies/<str:code>/update/', views.currency_update, name='currency_update'),
    path('currencies/<str:code>/delete/', views.currency_delete, name='currency_delete'),

]