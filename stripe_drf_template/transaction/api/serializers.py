from django.db.models import fields
from django.db.models.fields import files
from rest_framework import serializers
from ..models import (
    Currency,
    Transaction,
)

class CurrencySerializer(serializers.ModelSerializer):
    
    """
    Serializer for currency CRUD.
    """
    
    class Meta:
        model = Currency
        fields = ['name', 'symbol', 'code']


class TransactionListDetailSerializer(serializers.ModelSerializer):

    """
    Serializer for the relevant fields to be displayed on a list
    and detail views.
    """
    
    currency = serializers.StringRelatedField()
    
    class Meta:
        model = Transaction
        fields = ['name', 'email', 'creation_date', 'payment_date', 'amount',
        'currency', 'payment_status', 'link', 'message', 'payment_charge_id']


class TransactionCreateSerializer(serializers.ModelSerializer):
    
    """
    Serializer for just the needed fields to create a transaction,
    plus an optional notes field.
    """

    currency = serializers.StringRelatedField()

    class Meta:
        model = Transaction
        fields = ['name', 'email', 'amount', 'currency', 'notes']


class TransactionUpdateSerializer(serializers.ModelSerializer):
    
    """
    Serializer for only the fields that should be updateable.
    """
    
    class Meta:
        model = Transaction
        fields = ['name', 'email', 'message', 'payment_status']