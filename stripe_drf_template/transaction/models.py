from django.db import models
from django.conf import settings
from django.utils import timezone
from stripe_drf_template.common.utils import encrypt, create_payment_intent



class Currency(models.Model):
    
    """
    - name: the full name of the currency (i.e.: US Dollar, Argentine Peso)
    - code: the international code of the currency (i.e.: USD, ARS)
    - symbol: the currencie's symbol (i.e.: $)
    """
    
    name    = models.CharField(max_length=20, null=False, blank=False, unique=True)
    code    = models.CharField(max_length=3, null=False, blank=False, unique=True)
    symbol  = models.CharField(max_length=5, null=False, blank=False, default='$')

    def __str__(self) -> str:
        return self.code

    class Meta:
        verbose_name_plural = "Currencies"



class Transaction(models.Model):
    
    """
    Model to represent transaction objects.
    """
    
    name                = models.CharField(max_length=50, null=False, blank=False)
    email               = models.EmailField(max_length=50, null=False, blank=False)
    creation_date       = models.DateTimeField(auto_now_add=True, null=False, blank=False)
    payment_date        = models.DateTimeField(null=True, blank=False)
    amount              = models.DecimalField(max_digits=7, decimal_places=2, null=False, blank=False)
    currency            = models.ForeignKey(Currency, null=False, blank=False, default=1, on_delete=models.PROTECT)
    stripe_response     = models.TextField(null=True, blank=False) # For referal
    payment_intent_id   = models.CharField(max_length=100, null=False, blank=False)
    billing_name        = models.CharField(max_length=50, null=True, blank=False)
    billing_email       = models.EmailField(max_length=50, null=True, blank=False)
    crypto              = models.CharField(max_length=44, null=False, blank=False)

    SUC = 'SUC'
    WAI = 'WAI'

    PAYMENT_STATUS_CHOICES = (
        (WAI, 'Waiting for payment'), # Status until payment has succeeded
        (SUC, 'Succeeded'),
    )

    payment_status      = models.CharField(choices=PAYMENT_STATUS_CHOICES, default=WAI, max_length=21)
    link                = models.CharField(max_length=255, null=True, blank=True)
    message             = models.TextField(null=True, blank=True)


    def amount_and_currency(self):
        return f'{self.amount} {self.currency.code.lower()}' 
    amount_and_currency.short_description = 'Amount'


    def save(self, **kwargs):

        """
        - If the object is created for the first time we fill the rest of the data
        beyond the fields passed inside the POST request that created this instance

        - This could also be replaced with two signals: a pre-save and post-save.
        """

        if not self.id: # If id == None that means the object is being created
            # ==> We fill the remaining fields <== #
            self.payment_intent_id, self.payment_charge_id = create_payment_intent(self.amount, self.currency)
            self.amount         = float(self.amount)
            self.creation_date  = timezone.now()
            self.billing_name   = self.name # Billing name will be the transactions's email name unless the mail that originated the payment isn't the mail of the person supposed to pay
            self.billing_email  = self.email # Same here
            
            # ==> We create the link field <== #
            super(Transaction, self).save() # We save it to generate an id to encrypt
            self.crypto         = encrypt(self.id)    
            self.link           = settings.APP_URL + self.crypto

        super(Transaction, self).save()