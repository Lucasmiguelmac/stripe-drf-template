from django.conf import settings
from django.test import client
import factory
import pytest
import stripe
import datetime


from django.conf import settings
from django.urls import reverse
from ..models import Currency, Transaction

stripe.api_key = settings.STRIPE_SECRET_KEY


@pytest.mark.django_db
@pytest.mark.parametrize('stripe_test_card, expected_final_code, expected_html',
    [
        ('4242424242424242', 200, 'successful'), # Test normal auth went right
        # ('4000002760003184', 200, 'successful'), # Test 3d auth went right (selenium frontend testing required)
        # ('4000008400001280', 409, 'processing'), # Processing error in 3d (selenium frontend testing required)
        ('4000000000009995', 206, 'insufficient'), # Card declined cause of insufficient funds
        ('4000000000009987', 206, 'lost'), # Card declined because owner declared it lost
        ('4000000000009979', 206, 'stolen'), # Card declined because owner declared it stolen
        ('4000000000000069', 206, 'expired'), # Card declined because it expired
        ('4000000000000127', 206, 'invalid'), # CVC code was incorrect
        ('4000000000000119', 206, 'processing'), # Processing error (try again or contact support)
        ('4242424242424241', 206, 'card number'), # Wrong card number introduced
    ]
)
def test_payment_processing(client, stripe_test_card, expected_final_code, expected_html,):
    
    """
    Test the whole flow of the app without interacting with the frontend.

    Structure of the test:
    1- Create currency
    2- Create transaction with that currency
    3- Post a credit card to the charge endpoint and associate it to the transaction created
    4- Assert the proper message has been responded.
    """
    
    create_currency_endpoint = reverse('restapi:currency_create')
    required_currency_data = {
        'name': 'Argentine Peso',
        'code': 'ars',
        'symbol': '$',
    }
    response = client.post(create_currency_endpoint, data=required_currency_data)

    currency_pk = Currency.objects.last().id
    create_transaction_endpoint = reverse('restapi:transaction_create')
    required_transaction_data = {
        'name': 'Lucas',
        'email': 'lucasmail@gmail.com',
        'amount': 123,
        'currency_id': currency_pk
    }
    client.post(create_transaction_endpoint, data=required_transaction_data)
    transaction = Transaction.objects.last()
    
    try:
        stripe_payment_method = stripe.PaymentMethod.create(
            type="card",
            card={
                "number": stripe_test_card,
                "exp_month": 9,
                "exp_year": datetime.datetime.now().year + 1,
                "cvc": "314",
            },
        )
        required_form_data = {
            'crypto_id': transaction.crypto,
            'payment_method_id': stripe_payment_method.id,
        }
        charge_transaction_endpoint = reverse('restapi:transaction_charge')
        response = client.post(charge_transaction_endpoint, data=required_form_data, )

        # Asserting all different card errors
        if response.status_code == expected_final_code:
            assert expected_html.encode('utf-8') in response.content
        else:
            assert response.status_code == expected_final_code
    except stripe.error.InvalidRequestError as e:
        assert e.error.code == 'incorrect_number'