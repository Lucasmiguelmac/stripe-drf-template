from django.urls.base import resolve
from django.urls import reverse


class TestEndpoints:
    
    """
    Tests that assert every given endpoint delivers
    to the proper view.
    """

    def test_create(self):
        path = reverse('transaction:currency_create')
        assert resolve(path).view_name == 'transaction:currency_create'
    
    def test_update(self):
        path = reverse('transaction:currency_update', kwargs={'code': 'asd'})
        assert resolve(path).view_name == 'transaction:currency_update'
    
    def test_delete(self):
        path = reverse('transaction:currency_delete', kwargs={'code': 'asd'})
        assert resolve(path).view_name == 'transaction:currency_delete'
    
    def test_list(self):
        path = reverse('transaction:currency_list')
        assert resolve(path).view_name == 'transaction:currency_list'
    
    def test_create(self):
        path = reverse('transaction:transaction_create')
        assert resolve(path).view_name == 'transaction:transaction_create'

    def test_detail(self):
        path = reverse('transaction:transaction_detail', kwargs={'crypto': 'dasdsadadfasdfa'})
        assert resolve(path).view_name == 'transaction:transaction_detail'
    
    def test_update(self):
        path = reverse('transaction:transaction_update', kwargs={'crypto': 'dasdsadadfasdfa'})
        assert resolve(path).view_name == 'transaction:transaction_update'
    
    def test_delete(self):
        path = reverse('transaction:transaction_delete', kwargs={'crypto': 'dasdsadadfasdfa'})
        assert resolve(path).view_name == 'transaction:transaction_delete'
    
    def test_list(self):
        path = reverse('transaction:transaction_list')
        assert resolve(path).view_name == 'transaction:transaction_list'