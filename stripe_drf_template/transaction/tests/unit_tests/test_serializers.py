from django_mock_queries.query import MockModel, MockSet, Mock
from itertools import chain
from ..conftest import CurrencyFactory, TransactionFactory, serializer_validation_cases

import pytest

from ...api.serializers import CurrencySerializer, TransactionListDetailSerializer, TransactionCreateSerializer, TransactionUpdateSerializer

class TestCurrencySerializer:

    def test_currency_serialization(self):
        serializer = CurrencySerializer
        expected_fields = ['name', 'symbol', 'code']
        
        assert serializer.Meta.fields == expected_fields
    

class TestTransactionSerializers:

    @pytest.mark.parametrize('serializer, expected_fields',
    [
        (
            TransactionListDetailSerializer,
            ['name', 'email', 'creation_date', 'payment_date', 'amount', 'currency', 'payment_status', 'link', 'message', 'payment_charge_id']
        ),
        (
            TransactionCreateSerializer,
            ['name', 'email', 'amount', 'currency_id', 'ref_code', 'message']
        ),
        (
            TransactionUpdateSerializer,
            ['name', 'email', 'message', 'payment_status']
        )
    ])
    def test_transaction_serialization(self, serializer, expected_fields):
        assert serializer.Meta.fields == expected_fields 
