from locale import currency
from PIL.Image import CUBIC
from mock import Mock
from server.apps.quickpay.scripts.unit import transactions_qs
from server.apps.Analytics import views
from django.core.exceptions import ValidationError
from django.urls.base import resolve
from django_mock_queries.query import MockModel, MockSet, Mock
from itertools import chain
from ..conftest import CurrencyFactory, TransactionFactory

import json
import pytest
from rest_framework import serializers
from rest_framework.response import Response

from rest_framework.exceptions import ValidationError
from django.urls import reverse
from ... import models
from ...models import Transaction, Currency
from ...api.serializers import CurrencySerializer, TransactionListDetailSerializer, TransactionCreateSerializer, TransactionUpdateSerializer
from ...api import views
from ...scripts.unit import to_dict


class TestCurrencyViews:
    def test_currency_list(self, mocker, rf):
        url = reverse('restapi:currency_list')
        request = rf.get(url)
        currency_queryset = MockSet(
            CurrencyFactory.build(),
            CurrencyFactory.build(),
            CurrencyFactory.build(),
        )
        
        mocker.patch('server.apps.quickpay.api.views.currencies_qs', return_value=currency_queryset)
        expected_json = [to_dict(t) for t in currency_queryset]
        mocker.patch('server.apps.quickpay.api.views.serialized_data', return_value=expected_json)

        response = views.currency_list(request).render()

        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3

    def test_currency_create(self, rf, mocker):
        url = reverse('restapi:currency_create')
        currency_object = CurrencyFactory.build()
        post_data = to_dict(currency_object)
        request = rf.post(url, post_data)

        mocker.patch.object(views, 'CurrencySerializer', Mock(CurrencySerializer))
        mocker.patch('rest_framework.serializers.BaseSerializer.is_valid', return_value=True)
        mocker.patch('rest_framework.serializers.BaseSerializer.save')

        response = views.currency_create(request).render()

        assert response.status_code == 201
        
    def test_currency_update(self, rf, mocker):
        currency_object = CurrencyFactory.build()
        url = reverse('restapi:currency_update', args=[currency_object.code])
        put_data = to_dict(currency_object)
        request = rf.put(url, put_data)

        # El problema está con request.data, lo tengo que arreglar patcheando CurrencySerializer
        mocker.patch('server.apps.quickpay.api.views.get_currency', return_value=currency_object)
        mocker.patch('server.apps.quickpay.api.views.CurrencySerializer')
        mocker.patch('rest_framework.serializers.BaseSerializer.is_valid', return_value=True)
        mocker.patch('rest_framework.serializers.BaseSerializer.save')
        response = views.currency_update(request, currency_object.code).render()

        assert response.status_code == 201





        
    # Currency update
    # def test_currency_update_works(self, rf, mocker):
    #     currency_object = Currency(name='dsa', code='asd', symbol='%', id=1)
    #     url = reverse('restapi:currency_update', args={'code': 'ars'})
    #     put_data = to_dict(currency_object)
    #     request = rf.put(url, put_data)
        
    #     mocker.patch('server.apps.quickpay.api.views.get_currency', return_value=currency_object)
    #     mocker.patch('server.apps.quickpay.api.views.CurrencySerializer', autospec=True)
    #     mocker.patch('rest_framework.serializers.BaseSerializer.is_valid', return_value=True)
    #     mocker.patch('rest_framework.serializers.BaseSerializer.save')

    #     response = views.currency_update(request, currency_object.code).render()

    #     assert response.status_code == 202

    # # Currency delete
    # def test_currency_list_works(self, rf, mocker):
    #     url = reverse('currency_list')
    #     request = rf.get(url)

   
class TestTransactionViews:
    
    # # Transaction Form Detail
    # def test_currency_list_works(self, rf, mocker):
    #     url = reverse('currency_list')
    #     request = rf.get(url)
    # Transaction List
    def test_transaction_list_works(self, rf, mocker):
        url = reverse('restapi:transaction_list')
        request = rf.get(url)
        transactions_queryset = MockSet(
            TransactionFactory.build(),
            TransactionFactory.build(),
            TransactionFactory.build()
        )
        
        mocker.patch('server.apps.quickpay.api.views.transactions_qs', return_value=transactions_queryset)
        expected_json = [to_dict(t) for t in transactions_queryset]
        mocker.patch('server.apps.quickpay.api.views.serialized_data', return_value=expected_json)

        response = views.transaction_list(request).render()

        assert response.status_code == 200
        assert len(json.loads(response.content)) == 3
    # def test_currency_list_works(self, rf, mocker):
    #     url = reverse('currency_list')
    #     request = rf.get(url)
    # # Transaction Create
    # def test_currency_list_works(self, rf, mocker):
    #     url = reverse('currency_list')
    #     request = rf.get(url)
    # # Transaction Update
    # def test_currency_list_works(self, rf, mocker):
    #     url = reverse('currency_list')
    #     request = rf.get(url)
    # # Transaction Delete
    # def test_currency_list_works(self, rf, mocker):
    #     url = reverse('currency_list')
    #     request = rf.get(url)
    # # Transaction Detail
    # def test_currency_list_works(self, rf, mocker):
    #     url = reverse('currency_list')
    #     request = rf.get(url)
