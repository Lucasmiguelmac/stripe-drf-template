import factory
from ..models import Transaction, Currency
from factory.fuzzy import FuzzyText
from ..scripts.unit import to_dict

class CurrencyFactory(factory.DjangoModelFactory):
    class Meta:
        model = Currency

    id = 1
    name = factory.Faker('name')
    code = FuzzyText(length=3)
    symbol = FuzzyText(length=1)

class TransactionFactory(factory.DjangoModelFactory):
    class Meta:
        model = Transaction
    name = factory.Faker('name')
    email = factory.Faker('email')
    amount =  factory.Faker('random_number')
    currency = factory.SubFactory(CurrencyFactory)



# ==> HELPER FUNCTIONS <==

def serializer_validation_cases(serializers):
    """
    Function that makes validation cases for all serializers, returning a list
    that contains a tuple that a serializer and an incomplete-data case.
    """
    sample_dict = to_dict(TransactionFactory.build())
    param_lst = []
    for serializer in serializers:
        serializer_fields = serializer.Meta.fields
        for i in range(4):

            incomplete_fields = serializer_fields[:i]+serializer_fields[i+1:]
            incomplete_data = {}
            for field in incomplete_fields:
                try:
                    incomplete_data[field] = sample_dict[field]
                except Exception:
                    pass
            param_lst.append(
                (serializer, incomplete_data)
            )
    return param_lst