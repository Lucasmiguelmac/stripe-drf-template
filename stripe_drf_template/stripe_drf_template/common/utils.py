from Crypto.Cipher.AES import block_size
from django.conf import settings
from Crypto.Cipher import AES
from Crypto import Random
import base64
import stripe


"""
Document intended for helper code a.k.a "utils"
"""

#############################################################################################
#############################################################################################

def encrypt(message):
    message = str(message)
    padded_message = message.encode("utf-8") + b'\0' * (
        AES.block_size - len(message) % AES.block_size
    )
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(settings.LOKAFY_KEY_CRYPTO[:16].encode("utf-8"), AES.MODE_CBC, iv)
    aes_cipher = iv + cipher.encrypt(padded_message)
    return base64.b64encode(aes_cipher).decode('utf-8')

#############################################################################################
#############################################################################################

def decrypt(message):
    base64_bytes = message.encode('latin-1')
    aes_bytes = base64.b64decode(base64_bytes)
    iv = aes_bytes[:AES.block_size]
    cipher = AES.new(settings.LOKAFY_KEY_CRYPTO[:16].encode('utf-8'), AES.MODE_CBC, iv)
    plain_text = cipher.decrypt(aes_bytes[AES.block_size:])
    return int(plain_text.rstrip(b"\0"))

#############################################################################################
#############################################################################################

stripe.api_key = settings.STRIPE_SECRET_KEY

def create_payment_intent(amount, currency):
    payment_intent = stripe.PaymentIntent.create(
                amount=int(amount * 100),
                currency=currency.code.lower(),
                payment_method_types=['card'],
            )
    return payment_intent.id, payment_intent.description

#############################################################################################
#############################################################################################

def serialized_data(serializer):
    return serializer.data

#############################################################################################
#############################################################################################

def to_dict(instance):
    """
    Turns model into a dict
    https://stackoverflow.com/a/29088221/12010568
    """
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields):
        data[f.name] = f.value_from_object(instance)
    for f in opts.many_to_many:
        data[f.name] = [i.id for i in f.value_from_object(instance)]
    return data

#############################################################################################
#############################################################################################