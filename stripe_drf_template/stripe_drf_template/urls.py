from django.contrib import admin
from django.urls import path, re_path
from django.urls.conf import include

from transaction.views import template

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('transaction.api.urls')),
    re_path(r'^(?P<crypto>.+?)/?$', template, name='transaction'),
]
